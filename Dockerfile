FROM openjdk:8-jdk-alpine
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","-Xmx512m","-Xms512m","/app.jar"]