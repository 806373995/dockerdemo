# dockerdemo 测试项目

dockerdemo 项目主要用于分布部署`mysql`和`web`服务,分别部署到 docker.

## web 模块架构

- springboot 项目启动
- springJPA 持久层框架
- openjdk8 java环境


# 配置流程

1. 配置 mysql 的 docker-compose
    1. 创建`docker-compose-mysql.yml`文件
    1. 配置一个`network`设置名称为demo-net
    1. 继续配置db相关端口,容器,文件映射
1. 配置 web 服务的端口
    1. 将`application.yml`的数据库链接的ip地址改为mysql的容器名称
1. 配置`Dockerfile`
    1. 配置使用`openjdk8`作为容器环境
    1. 复制jar到容器
    1. 运行jar的相关指令
1. 配置 web 的`docker-compose`
    1. 创建`docker-compose-server.yml`文件
    1. 配置一个`network`,使用的名称需要和mysql compose文件里面的network名称相同,并且需要指定为`桥接模式`
    1. 配置使用`Dockerfile`文件作为启动方式来启动

## 运行流程
1. 使用`docker-compose -f docker-compose-mysql.yml up -d --build`指令运行，创建mysql容器并且同时创建了一个network.
2. 修改 application.yml 中的 MySQL 链接，配置 ip 地址为 mysql 的容器名称，或者 db.
3. 打包项目 jar
4. 使用`docker-compose -f docker-compose-server.yml up -d --build`指令运行,创建jre容器，运行jar包，链接mysql容器.


## Q&A
1. web 服务如何调用 mysql

    将 web 容器和 mysql 容器配置到同一个 network 当中.多个服务在同一个 network 当中,他们的端口都能够访问,但是 ip 地址未知.
    network 有域名解析的机制,如果服务之间需要远程调用,只需要把被调用服务的容器名称当作 ip 地址,就能够访问需要的服务.