package com.test.dockerdemo;

import com.test.dockerdemo.dao.UserDao;
import com.test.dockerdemo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Boot {

    @Autowired
    UserDao userDao;

    public static void main(String[] args) {
        SpringApplication.run(Boot.class, args);
    }

    @GetMapping(path = "get",produces = "application/json;charset=UTF-8")
    public User getUser(){
        User user=userDao.getOne(1L);
        System.out.println(user);
        return user;
    }

}
